from game import Game

import random
import os
import time

def getPosX(turn, players, color):
    x = ''
    while not x.isnumeric():
        x = input(f'{players[turn].getName()} ({players[turn].getColor()}•{color.DEFAULT}), choose a column: ')
        if x == 'exit': exit()
    return int(x)

def changeTurn(turn):
    return 0 if turn == 1 else 1

def displayWinner(winner, color):
    if not winner:
        print('No one win this game.')
    else:
        print(f"The winner is {winner.getColor()}{winner.getName()}{color.DEFAULT} !")

def newGame(game:Game, players, color):
    
    turn = random.randint(0, 1)

    # boucle d'un jeu
    while game.isLaunch():

        os.system('cls')
        game.display()

        # demande à l'utilisateur où il veut placer son pion
        x = getPosX(turn, players, color)

        # vérification de si il peut le placer
        canPlace = game.set(players[turn], (x - 1) % 7)
        if not canPlace:
            print("This column is full. You can't play here !")
            time.sleep(1)
        else:
            # check si il y a un gagnant et si il le jeu est fini (plus de place)
            winner = game.hasWinner()
            game.isFull()

            # changementde joueur
            turn = changeTurn(turn)

    return winner

def launchGame(players, color):

    game = Game()
    winner = newGame(game, players, color)
    playList = ['yes', 'y', 'o', 'oui']

    # ecran de fin de jeu avec l'affichage des logs
    game.display()
    displayWinner(winner, color)
    
    time.sleep(1)
    os.system('cls')
    print('Logs:')
    print(open('./log.txt', 'r').read())

    # demande si il veut rejouer
    if input("Play again ? (yes/no) -> ").lower() in playList: launchGame(players, color)