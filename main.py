from player import Player
from color import Color

import os
import time
from progress.spinner import MoonSpinner

def setName():
    return [input('Player1 choose your name: '), input('Player2 choose your name: ')]

def setColor(playerName):
    color.display()
    lst = []
    print()
    for i in range(2):
        x = ''
        while not x.upper() in color.__dict__.keys():
            x = input(playerName[i] + " choose your color from the one above: ")
            if x == 'exit': exit()
        lst.append(x)
    return lst

def setPlayer():
    playerName = setName()
    os.system('cls')
    playerColor = setColor(playerName)
    return [Player(playerName[0], color.getValue(playerColor[0])), Player(playerName[1], color.getValue(playerColor[1]))]

def gamemode():
    mode = ''
    while mode != 'cli' and mode != 'graphic':
        mode = input("Choisissez le mode de jeux (cli/graphic): ")
        if mode == 'exit': exit()
    return mode

# écran de lancement du jeu
def loadingScreen():
    print(color.PURPLE)
    print(" /$$$$$$$            /$$                                                                  /$$   /$$")
    print("| $$__  $$          |__/                                                                 | $$  | $$")
    print("| $$  \ $$ /$$   /$$ /$$  /$$$$$$$ /$$$$$$$  /$$$$$$  /$$$$$$$   /$$$$$$$  /$$$$$$       | $$  | $$")
    print("| $$$$$$$/| $$  | $$| $$ /$$_____//$$_____/ |____  $$| $$__  $$ /$$_____/ /$$__  $$      | $$$$$$$$")
    print("| $$____/ | $$  | $$| $$|  $$$$$$|  $$$$$$   /$$$$$$$| $$  \ $$| $$      | $$$$$$$$      |_____  $$")
    print("| $$      | $$  | $$| $$ \____  $$\____  $$ /$$__  $$| $$  | $$| $$      | $$_____/            | $$")
    print("| $$      |  $$$$$$/| $$ /$$$$$$$//$$$$$$$/|  $$$$$$$| $$  | $$|  $$$$$$$|  $$$$$$$            | $$")
    print("|__/       \______/ |__/|_______/|_______/  \_______/|__/  |__/ \_______/ \_______/            |__/")
    print(color.DEFAULT)

# bar d chargement
def loadingBar():
    with MoonSpinner(color.YELLOW + 'Loading..') as bar:
        for i in range(100):
            time.sleep(0.02)
            bar.next()
    print(color.DEFAULT)

# initialisation des prérequis
color = Color()

loadingScreen()
loadingBar()
os.system('cls')
players = setPlayer()
mode = gamemode()
if mode == 'cli':
    from cli import *
    launchGame(players, color)
elif mode == 'graphic':
    from graphic import *
    launchGame(players)
else:
    pass