class Player():

    def __init__(self, name:str, color:str) -> None:
        self.name = name
        self.color = color

    def getName(self) -> str: return self.name
    
    def getColor(self) -> str: return self.color