# Puissance 4

This is a simple puissance4 for a school exercice.

# How to use it ?

Simply download it and launch `main.py` file.
You need to download the following different dependencies:
```
> pip install time
> pip install os
> pip install random
> pip install progress progressbar2 alive-progress tqdm
```