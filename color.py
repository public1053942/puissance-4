class Color():

    def __init__(self) -> None:
        self.RED = '\33[31m'
        self.GREEN = '\33[32m'
        self.YELLOW = '\33[33m'
        self.BLUE = '\33[34m'
        self.PURPLE = '\33[35m'
        self.DEFAULT = '\33[0m'

    def getName(self, value):
        for color in self.__dict__.keys():
            if self.__dict__[color] == value:
                return color.lower()
            
    def getValue(self, value):
        for color in self.__dict__.keys():
            if color == value.upper():
                return self.__dict__[color]
            
    def display(self):
        for color in self.__dict__.keys():
            print('- ' + color.lower())

    def getRGB(self, value):
        dic = {
            "red": (213, 48, 0),
            "green": (45, 213, 0),
            "blue": (0, 251, 255),
            "purple": (131, 0, 255),
            "yellow": (210, 243, 0)
        }
        return dic[value]