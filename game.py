from player import Player
from color import Color

class Game():

    def __init__(self) -> None:
        self.board = [[None for i in range(7)] for j in range(6)]
        self.launch = True
        self.path = './log.txt'
        open(self.path, 'w').close()

    def getBoard(self) -> list:
        return self.board

    # place le pion du joueur dans la colonne souhaité si elle n'est pas remplie
    def set(self, player, x) -> bool:
        y = self.getPos(x)
        if isinstance(y, bool): return False
        open(self.path, 'a').write(f"name={player.getName()}, color={Color().getName(player.getColor())}, pos=({x}, {y})\n")
        self.board[y][x] = player
        return True

    # récupère la position la plus basse jouable dans une colonne
    def getPos(self, x) -> int | bool:
        for i in range(len(self.board) - 1, -1, -1):
            if self.board[i][x] == None:
                return i
        return False

    def isLaunch(self) -> bool: return self.launch

    # affiche le plateau de jeu
    def display(self) -> None:
        msg =  '-----------------------------'
        for ligne in self.board:
            msg += '\n|'
            for colonne in ligne:
                toDisplay = f'{colonne.getColor()}•{Color().DEFAULT}' if colonne != None else '-'
                msg += f' {toDisplay} |'
        msg +=  '\n-----------------------------\n| 1 | 2 | 3 | 4 | 5 | 6 | 7 |\n'
        print(msg)

    # check si le plateau n'est pas full
    def isFull(self) -> bool:
        for e in self.board[0]:
            if e == None: return False
        self.launch = False
        return True

    # check si un joueur à gagné
    def hasWinner(self) -> bool | Player:

        # horizontal
        for ligne in self.board:
            for i in range(len(ligne) - 3):
                winner = []
                for j in range(4):
                    winner.append(ligne[i+j])
                if (winner[0] == winner[1] == winner[2] == winner[3]) and winner[0] != None:
                    self.launch = False
                    return winner[0]
                    
        # vertical
        for i in range(len(self.board[0])):
            for j in range(len(self.board) - 3):
                winner = []
                for k in range(4):
                    winner.append(self.board[j+k][i])
                if (winner[0] == winner[1] == winner[2] == winner[3]) and winner[0] != None:
                    self.launch = False
                    return winner[0]

        # diagonale 1
        for j in range(3):
            for i in range(4):
                if (self.board[0+j][0+i] == self.board[1+j][1+i] == self.board[2+j][2+i] == self.board[3+j][3+i]) and self.board[0+j][0+i] != None:
                        self.launch = False
                        return self.board[0+j][0+i]

        # diagonale 2
        for j in range(3):
            for i in range(4):
                if (self.board[3+j][0+i] == self.board[2+j][1+i] == self.board[1+j][2+i] == self.board[0+j][3+i]) and self.board[3+j][0+i] != None:
                        self.launch = False
                        return self.board[3+j][0+i]

        return False