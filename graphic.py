import random
import time
import os

from color import Color
from game import Game
from player import Player

import pygame
import pygame_widgets
from pygame_widgets.slider import Slider

def update(screen:pygame.Surface, game:Game):
    i = 0
    for row in game.getBoard():
        for case in row:
            if case == None:
                image = pygame.image.load('./image/none.png')
            else:
                image = pygame.image.load(f"./image/{Color().getName(case.getColor())}.png")
            
            position = (4 + (50 * (i % 7)), 4 + (50 * (i // 7)))
            screen.blit(image, dest = position)
            i+=1

def launchGame(players):
    
    pygame.init()
    game = Game()

    window = pygame.display.set_mode((350, 400))
    pygame.display.set_caption("Puissance 4")

    image = pygame.image.load('./image/background.png')
    position = (0, 0)

    turn = random.randint(0, 1)
    slider = Slider(window, 25, 315, 301, 20, min=1, max=7, step=1, colour=(0, 0, 150), handleColour=(255, 255, 255))

    placeButton = pygame.image.load("image/place.png")
    placeButton = pygame.transform.scale(placeButton, (100, 30))
    placeButton_rect = placeButton.get_rect()
    placeButton_rect.x = (window.get_size()[0] - placeButton.get_size()[0]) / 2
    placeButton_rect.y = 350

    while True:
        
        slider.handleColour = Color().getRGB(Color().getName(players[turn].getColor()))

        window.fill((30, 30, 255))
        window.blit(image, dest = position)
        window.blit(placeButton, placeButton_rect)
        update(window, game)

        events = pygame.event.get()
        for event in events: 
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            elif event.type == pygame.MOUSEBUTTONDOWN and placeButton_rect.collidepoint(event.pos):

                placeButton = pygame.image.load("image/place.png")
                canPlace = game.set(players[turn], slider.getValue() - 1)
                if not canPlace:
                    turn = 0 if turn == 1 else 1
                    print("This column is full, please choose an empty one !")
                turn = 0 if turn == 1 else 1
                winner = game.hasWinner()
                game.isFull()

                if not game.isLaunch():

                    update(window, game)
                    pygame.display.update()

                    if winner:
                        print(f"The winner is {winner.getColor() + winner.getName() + Color().DEFAULT} !")
                    else:
                        print("No one win..")

                    time.sleep(2)
                    os.system('cls')
                    print('Logs:')
                    print(open('log.txt', 'r').read())
                    
                    slider = None
                    playAgain(window, players, winner)
                    break

            elif event.type == pygame.MOUSEMOTION:
                placeButton = pygame.image.load("image/place#hover.png") if placeButton_rect.collidepoint(event.pos) else pygame.image.load("image/place.png")
        
        pygame_widgets.update(events)
        pygame.display.update()

def playAgain(screen:pygame.Surface, players: list[Player], winner: bool|Player):
    
    playAgainButton = pygame.image.load("image/playAgain.png")
    playAgainButton = pygame.transform.scale(playAgainButton, (100, 30))
    playAgainButton_rect = playAgainButton.get_rect()
    playAgainButton_rect.x = 65
    playAgainButton_rect.y = 350


    font = pygame.font.Font(pygame.font.get_default_font(), 24)
    text_winner = f"The winner is {winner.getName()} !" if winner else "No one win.."
    text_surface = font.render(text_winner, True, (255, 255, 255))

    quitButton = pygame.image.load("image/quit.png")
    quitButton = pygame.transform.scale(quitButton, (100, 30))
    quitButton_rect = quitButton.get_rect()
    quitButton_rect.x = 185
    quitButton_rect.y = 350

    while True:

        screen.fill((30, 30, 255))
        screen.blit(playAgainButton, playAgainButton_rect)
        screen.blit(quitButton, quitButton_rect)
        screen.blit(text_surface, ((screen.get_size()[0] - text_surface.get_size()[0]) / 2, (screen.get_size()[1] - text_surface.get_size()[1]) / 2))

        events = pygame.event.get()
        for event in events: 
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if playAgainButton_rect.collidepoint(event.pos):
                    launchGame(players)
                    break
                elif quitButton_rect.collidepoint(event.pos):
                    pygame.quit()
                    exit()
            elif event.type == pygame.MOUSEMOTION:
                playAgainButton = pygame.image.load("image/playAgain#hover.png") if playAgainButton_rect.collidepoint(event.pos) else pygame.image.load("image/playAgain.png")
                quitButton = pygame.image.load("image/quit#hover.png") if quitButton_rect.collidepoint(event.pos) else pygame.image.load("image/quit.png")
        

        pygame.display.update()


if __name__ == "__main__":
    import player
    launchGame([player.Player("Namania", Color().getValue('red')), player.Player("Ostara", Color().getValue('yellow'))])